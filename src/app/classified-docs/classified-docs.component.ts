import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classified-docs',
  templateUrl: './classified-docs.component.html',
  styleUrls: ['./classified-docs.component.css']
})
export class ClassifiedDocsComponent implements OnInit {
docs:Observable<any>;

  constructor(public docService:ClassifyService ,private route:ActivatedRoute, public router:Router) { }

  ngOnInit() {
    this.docs = this.docService.getDocs();  
  }

}
