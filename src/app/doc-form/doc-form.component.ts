import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassifyService } from '../classify.service';

@Component({
  selector: 'app-doc-form',
  templateUrl: './doc-form.component.html',
  styleUrls: ['./doc-form.component.css']
})
export class DocFormComponent implements OnInit {

  constructor(private classifyService:ClassifyService, 
    private router:Router) { }

  
text:string;

  onSubmit(){
    this.classifyService.doc=this.text;
    this.router.navigate(['/classified']);
    }
  ngOnInit() {
   
  }
 
  

}



 
   






