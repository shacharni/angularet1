import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  category:string = "Loading...";
 
  text:string;
  changeCategory:string;
  categories:object = [{name: 'business'}, {name: 'entertainment'},{ name: 'politics'}, {name: 'sport'},{name: 'tech'}];

  constructor(public classifyService:ClassifyService,
     public router:Router) {}

ngOnInit() {
this.classifyService.classify().subscribe(
res => {
console.log(res);
console.log(this.classifyService.categories[res])
this.category = this.classifyService.categories[res];


console.log(this.classifyService.doc)
}
)
}
change(){
this.category=this.changeCategory;
}
onclick(){

console.log(this.category,this.classifyService.doc)
this.classifyService.addDoc(this.category,this.classifyService.doc);
this.router.navigate(['/classifieddocs']);
}


}



 

  
 