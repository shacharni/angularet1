import { AuthService } from './../auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
email:string;
password:string;
  constructor(public router:Router,public authservice:AuthService ,private route:ActivatedRoute) { }

  ngOnInit() {
  }

  onSubmit(){
    this.authservice.signUp(this.email,this.password);
    this.router.navigate(['/done']);
  }

  }


