import { logging } from 'protractor';
import { User } from './interfaces/user';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  user: Observable<User | null>
constructor(public afAuth:AngularFireAuth,
 
 private router:Router) { 
  this.user=this.afAuth.authState;
}

  signUp(email:string,password:string){
    this.afAuth.auth.createUserWithEmailAndPassword(email,password)
    .then(()=>{
      this.router.navigate(['/books']);
    }).catch((error) => {
      window.alert(error.message)
    })
     }

 login(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['/books']);
      }).catch((error) => {
        window.alert(error.message)
      })
       }
  logout(){
    this.afAuth.auth.signOut().then(res=>console.log('successful logout'));
  }
}
