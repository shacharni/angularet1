
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule, Routes } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import { FormsModule }   from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { SeccloginComponent } from './secclogin/secclogin.component';
import { ClassifyComponent } from './classify/classify.component';
import { DocFormComponent } from './doc-form/doc-form.component';
import { HttpClientModule } from '@angular/common/http';
import { ClassifiedDocsComponent } from './classified-docs/classified-docs.component';
import { AngularFireAuthModule } from '@angular/fire/auth';


const appRoutes: Routes = [
  //{ path: 'books', component: BooksComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'login', component: LoginComponent },
  { path: 'done', component: SeccloginComponent},
  { path: 'docform', component: DocFormComponent},
  { path: 'classified', component: ClassifyComponent},
  { path: 'classifieddocs', component:ClassifiedDocsComponent},
  { path: "",
  redirectTo: '/done',
  pathMatch: 'full'
},
];


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignUpComponent,
    LoginComponent,
    SeccloginComponent,
    ClassifyComponent,
    DocFormComponent,
    ClassifiedDocsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
 MatSelectModule,
 MatInputModule,
 MatCardModule,
 FormsModule,
 MatExpansionModule,
 HttpClientModule ,
 AngularFirestoreModule,
 AngularFireAuthModule ,
    RouterModule.forRoot(
      appRoutes,
      
     { enableTracing: true } // <-- debugging purposes only
    ),
    AngularFireModule.initializeApp(environment.firebaseConfig,'angularET1_1'),
    AngularFireModule,
    
  ],
  

  providers: [AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
